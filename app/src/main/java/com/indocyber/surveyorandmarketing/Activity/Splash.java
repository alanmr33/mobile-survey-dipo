package com.indocyber.surveyorandmarketing.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.co;
import com.indocyber.surveyorandmarketing.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class Splash extends AppCompatActivity {

    private TextView textLoading;
    private int currentStep=1;
    private boolean permissionRequest=true;
    public boolean syncStatus=false;
    public String syncMessage="";
    private boolean syncDb;
    private PowerManager.WakeLock wl;
    private JSONArray listObject;
    private long sizeData;

    //TODO these are variables
    private boolean isConfigured = false;
    public static boolean isSynced = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.indocyber.surveyorandmarketing.R.layout.activity_splash);
        textLoading= (TextView) findViewById(R.id.textViewLoading);
//        try {
//            /*
//            set hash for App
//             */
//            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                session.setHash(Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//            setStep(1);
//        } catch (PackageManager.NameNotFoundException e) {
//            Log.e(getClass().getSimpleName(),e.getMessage());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e(getClass().getSimpleName(),e.getMessage());
//        }
        // save power manager
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Iglo");
        wl.acquire();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!syncDb){
            setStep(1);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /*
    step splash
     */
    private void setStep(final int step){
        currentStep=step;
        switch (step){
            case 1 :
                textLoading.setText("Check Permission ...");
                permitStep();
                break;
            case 2 :
                textLoading.setText("Check App Configuration ...");
                configStep();
                break;
            case 3 :
                textLoading.setText("Load User Information ...");
                loginStep();
                break;
            case 4 :
                textLoading.setText("Get Data From Server ...");
                syncStep();
                break;
        }
    }
    /*
    step permission 1
     */
    private void permitStep(){
        if(checkPermission()){
            setStep(2);
        }else{
            /*
            marshmallow above
             */
            if(Build.VERSION.SDK_INT>=22){
                requestPermissions();
            }else{
                permissionRequest=false;
                setStep(2);
            }
        }
    }
    /*
    step configuration 2
     */
    private void configStep(){
        //TODO import data from SURVEYOR SETTING
        if (isConfigured) {
            setStep(3);
        }
        else{
            isConfigured = true; //TODO this is placeholder code
            setStep(2);
        }
    }
    /*
    step login checking 3
     */
    private void loginStep(){

        if (Login.isLogin) {
            setStep(4);
        }
        else{
            Intent login = new Intent(this, Login.class);
            startActivity(login);
        }
    }
    /*
    step sync database 4
     */
    private void syncStep(){
        sizeData=0;
        //if already synced
        if(isSynced){
            Intent main = new Intent(this,Main.class);
            startActivity(main);
            finish();
        }else {
            sync();
        }
    }
    /*
    step sync 5 check sync
     */
    public void checkSync(){
        /*
        if sync success
         */
        if(syncStatus){
            Intent main = new Intent(this,Main.class);
            startActivity(main);
            finish();
        }
        //TODO: take a look at what this code is doing with Configuration.java
        else{
            syncStatus = true;
            setStep(4);
        }
    }
    /*
    check permission in realtime
     */
    private Boolean checkPermission(){
        int phoneState= ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        int storagePermission= ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int storagePermissionW= ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraP= ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int locationCoarse=ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationFine=ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(phoneState==PackageManager.PERMISSION_GRANTED && storagePermission==PackageManager.PERMISSION_GRANTED && locationCoarse==PackageManager.PERMISSION_GRANTED
                && locationFine==PackageManager.PERMISSION_GRANTED && storagePermissionW==PackageManager.PERMISSION_GRANTED
                &&cameraP==PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }
    }
    //Requesting permission
    private void requestPermissions() {
        if (permissionRequest) {
            permissionRequest=false;
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_PHONE_STATE}, 1000);
        }
    }
    /*
    on get permission from intent
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if(requestCode==1000){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED){
                setStep(2);
            }else{
                Toast.makeText(this,"Please Grant Our Permission !",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
    public void sync(){
        syncDb=true;
        isSynced = true;
        checkSync();
    }
}
