package com.indocyber.surveyorandmarketing.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.surveyorandmarketing.R;

public class Main extends AppCompatActivity implements View.OnClickListener{
    private RelativeLayout relativeLayout;
    private TextView txtLoginAs;
    private ImageView menuNewOrder;
    private ImageView menuSavedOrder;
    private ImageView menuSentOrder;
    private ImageView menuPendingOrder;
    private ImageView menuAccount;
    private ImageView menuLogout;

    private int resultSum;
    private TextView textNotifBubble;
    private ImageView imgNotifBubble;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        init component
         */
        txtLoginAs= (TextView) findViewById(R.id.textViewLoginAs);
        menuNewOrder= (ImageView) findViewById(R.id.menuNewOrder);
        menuSavedOrder= (ImageView) findViewById(R.id.menuSavedOrder);
        menuSentOrder= (ImageView) findViewById(R.id.menuSentOrder);
        menuPendingOrder= (ImageView) findViewById(R.id.menuPendingOrder);
        menuAccount= (ImageView) findViewById(R.id.menuAccount);
        menuLogout= (ImageView) findViewById(R.id.menuLogout);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);

        /*
        set listener click here
         */
        menuAccount.setOnClickListener(this);
        menuNewOrder.setOnClickListener(this);
        menuPendingOrder.setOnClickListener(this);
        menuSavedOrder.setOnClickListener(this);
        menuSentOrder.setOnClickListener(this);
        menuLogout.setOnClickListener(this);
    }


    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            finishAffinity();
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.menuNewOrder :
                Intent newOrder=new Intent(this, NewOrder.class);
                startActivity(newOrder);
                break;
            case R.id.menuSavedOrder :
                Intent saveOrder=new Intent(this,SavedOrder.class);
                startActivity(saveOrder);
                break;
            case R.id.menuSentOrder :
                Intent sentOrder=new Intent(this,SentOrder.class);
                startActivity(sentOrder);
                break;
            case R.id.menuPendingOrder :
                Intent pendingOrder=new Intent(this, PendingOrder.class);
                startActivity(pendingOrder);
                break;
            case R.id.menuAccount :
                Intent userConfig=new Intent(this,UserSettings.class);
                startActivity(userConfig);
                break;
            case R.id.menuLogout :
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
//                                realm.beginTransaction();
//                                realm.deleteAll();
//                                realm.commitTransaction();
//                                session.setLogin(false);
//                                session.setSynced(false);
                                Login.isLogin = false;
                                Splash.isSynced = false;
                                Intent logout = new Intent(getApplicationContext(),Splash.class);
                                startActivity(logout);
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                break;
        }
    }
}
