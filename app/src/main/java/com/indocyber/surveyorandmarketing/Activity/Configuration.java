package com.indocyber.surveyorandmarketing.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.surveyorandmarketing.R;

public class Configuration extends AppCompatActivity implements View.OnClickListener{
    public String serverURLString = String.valueOf(R.string.server_url_hint);
    public TextView txtDevID,txtAppID;
    public EditText serverURL;
    public Button buttonSave;
    public ImageButton buttonConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        //init component
        serverURL= (EditText) findViewById(R.id.editTextURL);
        buttonSave= (Button) findViewById(R.id.buttonSaveConfiguration);
        buttonConnection= (ImageButton) findViewById(R.id.imageButtonTestConnection);
        txtDevID= (TextView) findViewById(R.id.textViewDevID);
        txtAppID= (TextView) findViewById(R.id.textViewAppID);
        //set on click button
        buttonSave.setOnClickListener(this);
        buttonConnection.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSaveConfiguration :
//                if(URLUtil.isValidUrl(serverURL.getText().toString())){
//                    //save config & back
//                    session.setURL(serverURL.getText().toString());
//                    session.setConfigured(true);
//                    finish();
//                }else{
//                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
//                }
                Toast.makeText(this,"Connection set (placeholder)",Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageButtonTestConnection :
//                if(URLUtil.isValidUrl(serverURL.getText().toString())) {
//                    testConnection(serverURL.getText().toString());
//                }else{
//                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
//                }
                Toast.makeText(this,"Testing connection (placeholder)",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
