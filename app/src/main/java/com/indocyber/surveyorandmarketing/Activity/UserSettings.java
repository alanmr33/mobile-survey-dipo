package com.indocyber.surveyorandmarketing.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.surveyorandmarketing.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class UserSettings extends AppCompatActivity implements View.OnClickListener{

    private EditText txtUserID,txtFullname,txtOPassword,txtNPassword,txtCPassword;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

         /*
        init all component
         */
        txtUserID= (EditText) findViewById(R.id.editTextUserID);
        txtFullname= (EditText) findViewById(R.id.editTextFullname);
        txtOPassword= (EditText) findViewById(R.id.editTextOPassword);
        txtNPassword= (EditText) findViewById(R.id.editTextNPassword);
        txtCPassword= (EditText) findViewById(R.id.editTextCPassword);
        btnSave= (Button) findViewById(R.id.buttonSaveSetting);
        /*
        set all on click listener
         */
        btnSave.setOnClickListener(this);

//        try {
//            txtUserID.setText(session.getUserdata().getString("Username"));
//            txtFullname.setText(session.getUserdata().getString("Fullname"));
//            txtUserID.setEnabled(false);
//        } catch (JSONException e) {
//            Log.e(getClass().getSimpleName(),e.getMessage());
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSaveSetting :
                Toast.makeText(this, "Information has been updated", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
