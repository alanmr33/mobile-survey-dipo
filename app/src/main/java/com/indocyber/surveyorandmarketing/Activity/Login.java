package com.indocyber.surveyorandmarketing.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.surveyorandmarketing.Activity.Splash;
import com.indocyber.surveyorandmarketing.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cz.msebera.android.httpclient.Header;

public class Login extends AppCompatActivity implements View.OnClickListener {
    private Button btnLogin;
    private EditText txtUsername,txtPassword;
//    private TextView txtError;

    public static boolean isLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*
        init component
         */
//        txtError= (TextView) findViewById(R.id.textViewError);
        btnLogin= (Button) findViewById(R.id.buttonLogin);
        txtUsername= (EditText) findViewById(R.id.editTextLoginUserID);
        txtPassword= (EditText) findViewById(R.id.editTextLoginPassword);
        //set onclick listener to this class
        btnLogin.setOnClickListener(this);
    }
    /*
    for all click event below
     */
    @Override
    public void onClick(View v) {
        /*
        choose event by element
         */
        switch (v.getId()){
            case R.id.buttonLogin:
                /*
                store text temp
                 */
                String username=txtUsername.getText().toString();
                String password=txtPassword.getText().toString();
                /*
                validate input
                 */
                if (username.equals("")){
                    txtUsername.setError("Username is required");
                    return;
                }
                if (password.equals("")){
                    txtPassword.setError("Password is required");
                    return;
                }
                isLogin = true;
                Intent intent = new Intent(this, Splash.class);
                startActivity(intent);
                break;
        }
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            finishAffinity();
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }
}
